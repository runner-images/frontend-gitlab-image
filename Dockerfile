FROM mcr.microsoft.com/dotnet/core/sdk:3.1-bionic

# Update apt-get
RUN apt-get update && apt-get install -y apt-utils && apt-get install -y curl

# Install build essentials, for compiling and stuff
RUN apt-get install -y build-essential
RUN apt-get install -y libgdiplus

# Install NPM
RUN curl -sL https://deb.nodesource.com/setup_11.x -o nodesource_setup.sh
RUN bash nodesource_setup.sh
RUN apt-get install -y nodejs

# Install Yarn
RUN npm i -g yarn

# Install CyanPrint
RUN npm i -g cyanprint@latest

# Install PngQuant
RUN apt-get install -y git libpng-dev
RUN git clone --recursive https://github.com/pornel/pngquant.git
WORKDIR pngquant
RUN make
RUN make install
WORKDIR ..	

# Install JpegOptim
RUN apt-get install -y jpegoptim

# Install deployimage
RUN dotnet tool install --global ImageDeployer --version 3.5.2

# Install dotnet cover
RUN dotnet tool install --global dotnet-reportgenerator-globaltool

# Install sonar scanner
RUN dotnet tool install --global dotnet-sonarscanner --version 4.7.1

# Install netdeploy
RUN yarn global add netlify-cli netlify-deploy testcafe webpack-cli

# Install chromium 
RUN apt-get install -y libxss1 libappindicator1 libindicator7
RUN wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
RUN apt install -y ./google-chrome*.deb

# Install firefox
RUN apt-get install -y firefox

# Export global tools
ENV PATH="${PATH}:/root/.dotnet/tools"

# Install all common packages for global caching
COPY . .
RUN yarn

# Install JRE
RUN apt install -y openjdk-11-jre